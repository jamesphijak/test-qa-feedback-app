DROP TABLE IF EXISTS questions;
CREATE TABLE questions (
  id TEXT PRIMARY KEY,
  message TEXT,
  created_date TIMESTAMPTZ,
  last_modified_date TIMESTAMPTZ,
  created_by TEXT,
  last_modified_by TEXT
);

DROP TABLE IF EXISTS answers;
CREATE TABLE answers (
  id TEXT PRIMARY KEY,
  question_id TEXT REFERENCES questions (id),
  message TEXT,
  created_date TIMESTAMPTZ,
  last_modified_date TIMESTAMPTZ,
  created_by TEXT,
  last_modified_by TEXT
);
-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO questions VALUES ('ce9cb692-baa8-4d9d-ba64-5ccc4bb729f0', 'Question 1', '2019-09-25 11:17:48.434+07', '2019-09-25 11:17:48.434+07', 'DGA Test User', 'DGA Test User');
INSERT INTO questions VALUES ('552fe7ac-766e-4cd0-9850-ed6421776a22', 'Question 2', '2019-09-25 11:17:56.358+07', '2019-09-25 11:17:56.358+07', 'DGA Test User', 'DGA Test User');
INSERT INTO questions VALUES ('4b184179-3a1e-4386-85f6-4d266aeb52e4', 'Question 3', '2019-09-25 11:17:59.469+07', '2019-09-25 11:17:59.469+07', 'DGA Test User', 'DGA Test User');
INSERT INTO questions VALUES ('53f793c7-0da6-4a1a-b4ea-24fc27a8e64e', 'Question 4', '2019-09-25 11:18:03.571+07', '2019-09-25 11:18:03.571+07', 'DGA Test User', 'DGA Test User');
INSERT INTO questions VALUES ('43a3c036-b006-490a-bf8a-a35327227fa8', 'Question 5', '2019-09-25 11:18:07.322+07', '2019-09-25 11:18:07.322+07', 'DGA Test User', 'DGA Test User');

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO answers VALUES ('6ed3c34d-5da2-428e-aa90-873e1e359f86', 'ce9cb692-baa8-4d9d-ba64-5ccc4bb729f0', 'Answer 1', '2019-09-25 11:18:57.318+07', '2019-09-25 11:18:57.318+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('58b48053-ca21-4de1-bee7-dfcdae884b92', 'ce9cb692-baa8-4d9d-ba64-5ccc4bb729f0', 'Answer 2', '2019-09-25 11:19:03.128+07', '2019-09-25 11:19:03.128+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('cb01128c-3c7d-4d17-8a79-ccc0746b207e', 'ce9cb692-baa8-4d9d-ba64-5ccc4bb729f0', 'Answer 3', '2019-09-25 11:19:06.59+07', '2019-09-25 11:19:06.59+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('59558656-405a-4149-872e-04fb00587faf', '552fe7ac-766e-4cd0-9850-ed6421776a22', 'Answer 1', '2019-09-25 11:21:15.162+07', '2019-09-25 11:21:15.162+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('f5dc5b59-ba9a-4376-a07c-c5189114dc59', '552fe7ac-766e-4cd0-9850-ed6421776a22', 'Answer 2', '2019-09-25 11:21:17.705+07', '2019-09-25 11:21:17.705+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('aa95ab4d-ee9f-4752-b67f-0c6d1cec5c3e', '552fe7ac-766e-4cd0-9850-ed6421776a22', 'Answer 3', '2019-09-25 11:21:20.496+07', '2019-09-25 11:21:20.496+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('8b1b9ad8-7a88-4820-832b-41984fe19457', '53f793c7-0da6-4a1a-b4ea-24fc27a8e64e', 'Answer 1', '2019-09-25 11:22:20.078+07', '2019-09-25 11:22:20.078+07', 'DGA Test User', 'DGA Test User');
INSERT INTO answers VALUES ('e3cd6cdc-1909-4820-b930-d81dd812898a', '53f793c7-0da6-4a1a-b4ea-24fc27a8e64e', 'Answer 2', '2019-09-25 11:22:23.121+07', '2019-09-25 11:22:23.121+07', 'DGA Test User', 'DGA Test User');
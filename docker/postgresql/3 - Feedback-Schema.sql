DROP TABLE IF EXISTS feedbacks;
CREATE TABLE feedbacks(
  id TEXT PRIMARY KEY,
  email TEXT,
  category TEXT,
  text TEXT,
  created_date TIMESTAMPTZ,
  last_modified_date TIMESTAMPTZ,
  created_by TEXT,
  last_modified_by TEXT
);
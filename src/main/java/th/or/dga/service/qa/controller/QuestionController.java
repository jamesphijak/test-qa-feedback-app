package th.or.dga.service.qa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import th.or.dga.service.exception.ResourceNotFoundException;
import th.or.dga.service.qa.model.Question;
import th.or.dga.service.qa.repository.QuestionRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionController {
    @Autowired
    private QuestionRepository questionRepository;
    public static final String ROLE_ADMIN = "ROLE_admin";

    @GetMapping("/questions")
    public List<Question> getAllQuestions() {
        return questionRepository.findAll();
    }

    @GetMapping("/questions/{questionId}")
    public Question getQuestionById(@PathVariable(value = "questionId") String questionId)
            throws ResourceNotFoundException {
        return questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    @Secured(ROLE_ADMIN)
    @PostMapping("/questions")
    public Question createQuestion(@Valid @RequestBody Question question) {
        return questionRepository.save(question);
    }

    @Secured(ROLE_ADMIN)
    @PutMapping("/questions/{questionId}")
    public Question updateQuestion(@PathVariable String questionId,
                                   @Valid @RequestBody Question questionRequest) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    question.setMessage(questionRequest.getMessage());
                    return questionRepository.save(question);
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    @Secured(ROLE_ADMIN)
    @DeleteMapping("/questions/{questionId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable String questionId) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }
}
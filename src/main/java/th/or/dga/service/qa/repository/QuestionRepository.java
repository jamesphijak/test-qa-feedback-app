package th.or.dga.service.qa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.or.dga.service.qa.model.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, String> {
}
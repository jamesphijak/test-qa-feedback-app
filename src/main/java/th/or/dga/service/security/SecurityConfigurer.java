package th.or.dga.service.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.jwt.Jwt;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
@AllArgsConstructor
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    private ObjectMapper mapper;

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        http.headers()
            .frameOptions().disable()
            .and()
            .csrf().disable()
            .sessionManagement().disable()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/questions/**").permitAll()
            .antMatchers(HttpMethod.POST, "/feedback").permitAll()
            .anyRequest().authenticated()
            .and()
            .oauth2ResourceServer()
            .jwt()
            .jwtAuthenticationConverter(grantedAuthoritiesExtractor());
    }

    @Bean
    public Converter<Jwt, AbstractAuthenticationToken> grantedAuthoritiesExtractor() {
        return new KeycloakJwtAuthenticationConverter(mapper);
    }

}

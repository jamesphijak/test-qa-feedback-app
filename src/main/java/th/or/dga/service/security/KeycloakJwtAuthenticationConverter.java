package th.or.dga.service.security;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class KeycloakJwtAuthenticationConverter extends JwtAuthenticationConverter {

    private static final String ROLE_PREFIX = "ROLE_";

    private static final String REALM_ELEMENT_IN_JWT = "realm_access";
    private static final String CLIENT_NAME_ELEMENT_IN_JWT = "resource_access";

    private static final String ROLES_ELEMENT_IN_JWT = "roles";

    private ObjectMapper mapper;

    protected Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
        JsonNode claims = mapper.convertValue(jwt.getClaims(), JsonNode.class);
        return extractRoles(claims).stream()
                                   .map(SimpleGrantedAuthority::new)
                                   .collect(Collectors.toList());
    }

    private Set<String> extractRoles(JsonNode jwt) {
        log.debug("Begin extractRoles: jwt = {}", jwt);

        Set<String> rolesWithPrefix = extractClientRoles(jwt);
        rolesWithPrefix.addAll(extractRealmRoles(jwt));

        log.debug("End extractRoles: roles = {}", rolesWithPrefix);
        return rolesWithPrefix;
    }

    private Set<String> extractClientRoles(JsonNode jwt) {
        Set<String> roles = new HashSet<>();

        jwt.path(CLIENT_NAME_ELEMENT_IN_JWT)
           .elements()
           .forEachRemaining(e -> e.path(ROLES_ELEMENT_IN_JWT)
                                   .elements()
                                   .forEachRemaining(r -> roles.add(ROLE_PREFIX + r.asText())));
        return roles;
    }

    private Set<String> extractRealmRoles(JsonNode jwt) {
        Set<String> roles = new HashSet<>();

        jwt.path(REALM_ELEMENT_IN_JWT)
           .elements()
           .forEachRemaining(e -> e.elements()
                                   .forEachRemaining(r -> roles.add(ROLE_PREFIX + r.asText())));
        return roles;
    }

}

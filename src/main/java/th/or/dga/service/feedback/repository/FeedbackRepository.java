package th.or.dga.service.feedback.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.or.dga.service.feedback.model.Feedback;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, String> {
}
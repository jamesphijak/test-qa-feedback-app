package th.or.dga.service.feedback.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import th.or.dga.service.exception.ResourceNotFoundException;
import th.or.dga.service.feedback.model.Feedback;
import th.or.dga.service.feedback.repository.FeedbackRepository;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
public class FeedbackController {
    @Autowired
    private FeedbackRepository feedbackRepository;
    public static final String ROLE_ADMIN = "ROLE_admin";

    @Secured(ROLE_ADMIN)
    @GetMapping("/feedback")
    public List<Feedback> getAllFeedback(HttpServletRequest request) {
        return feedbackRepository.findAll();
    }

    @Secured(ROLE_ADMIN)
    @GetMapping("/feedback/{feedbackId}")
    public Feedback getFeedbackById(@PathVariable(value = "feedbackId") String feedbackId)
            throws ResourceNotFoundException {
        return feedbackRepository.findById(feedbackId)
                .orElseThrow(() -> new ResourceNotFoundException("Feedback not found with id " + feedbackId));
    }

    @PostMapping("/feedback")
    public Feedback createFeedback(@Valid @RequestBody Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    @Secured(ROLE_ADMIN)
    @PutMapping("/feedback/{feedbackId}")
    public Feedback updateFeedback(@PathVariable String feedbackId,
                                   @Valid @RequestBody Feedback feedbackRequest) {
        return feedbackRepository.findById(feedbackId)
                .map(feedback -> {
                    feedback.setEmail(feedbackRequest.getEmail());
                    feedback.setCategory(feedbackRequest.getEmail());
                    feedback.setText(feedbackRequest.getText());
                    return feedbackRepository.save(feedback);
                }).orElseThrow(() -> new ResourceNotFoundException("Feedback not found with id " + feedbackId));
    }

    @Secured(ROLE_ADMIN)
    @DeleteMapping("/feedback/{feedbackId}")
    public ResponseEntity<?> deleteFeedback(@PathVariable String feedbackId) {
        return feedbackRepository.findById(feedbackId)
                .map(feedback -> {
                    feedbackRepository.delete(feedback);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Feedback not found with id " + feedbackId));
    }
}

package th.or.dga.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import th.or.dga.service.repository.JpaAuditorImpl;


@SpringBootApplication
public class QAServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(QAServiceApplication.class, args);
    }
}

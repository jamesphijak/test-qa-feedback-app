package th.or.dga.service.repository;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public class JpaAuditorImpl implements AuditorAware<String> {

    private static final String JWT_USERNAME_ATTRIBUTE = "preferred_username";

    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(securityContext -> {
                    String userName = "anonymous";
                    Authentication authentication = securityContext.getAuthentication();
                    if (authentication instanceof JwtAuthenticationToken) {
                        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
                        userName = (String) jwtAuthenticationToken.getTokenAttributes()
                                .get(JWT_USERNAME_ATTRIBUTE);
                    }
                    return userName;
                });
    }
//    @Override
//    public Optional<String> getCurrentAuditor() {
//        return Optional.of("DGA Test User");
//        // Use below commented code when will use Spring Security.
//    }
}


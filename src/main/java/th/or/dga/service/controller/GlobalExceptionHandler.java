package th.or.dga.service.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import th.or.dga.service.exception.ResourceNotFoundException;
import th.or.dga.service.dto.ErrorDetails;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDetails resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        return ErrorDetails.builder().timestamp(new Date()).message(ex.getMessage()).details(request.getDescription(false)).build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDetails globeExceptionHandler(Exception ex, WebRequest request) {
        return ErrorDetails.builder().timestamp(new Date()).message(ex.getMessage()).details(request.getDescription(false)).build();
    }
}
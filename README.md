# DGA QA Service
## 📘 About # DGA QA Service
This system can use api to manage questions and answers.

## 📦 Built With
- [X] Spring Boot
- [X] PostgreSQL

## PostgreSQL (Docker) Connection details
- [X] Server Type    : PostgreSQL
- [X] Host           : localhost:5432
- [X] Datebase       : ckan
- [X] Username       : ckan
- [X] Password       : ckan
- [X] Schema         : public